package mx.ipn.escom.wad;


    import java.io.*;  
    import javax.servlet.*;  
    import javax.servlet.http.*;  
    import java.util.*; 
    import java.util.Enumeration;
      
    public class ShowHeaders extends HttpServlet {  
      
    	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
    		doPost(request, response);
    	}

    	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {

    		response.setContentType("text/html");
    		PrintWriter out = response.getWriter();
    		out.println("Headers<hr/>");
    		Enumeration<String> headerNames = request.getHeaderNames();
    		while (headerNames.hasMoreElements()) {
    			String headerName = headerNames.nextElement();
    			out.print("Header Name: <em>" + headerName);
    			String headerValue = request.getHeader(headerName);
    			out.print("</em>, Header Value: <em>" + headerValue);
    			out.println("</em><br/>");
    		}

    	}
    }  