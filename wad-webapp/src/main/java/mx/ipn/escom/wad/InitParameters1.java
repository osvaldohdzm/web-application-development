package mx.ipn.escom.wad;

import java.io.IOException;
import java.io.PrintWriter;
 
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
 
public class InitParameters1 extends HttpServlet {
	private static final long serialVersionUID = 1L;
 
	protected void doGet(HttpServletRequest request,
		HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		out.println("Parámetro 1:" + this.getInitParameter("parametro1"));
		out.println("Parámetro 2:" + this.getInitParameter("parametro2"));
		out.println("Parámetro 3:" + this.getInitParameter("parametro3"));
		//out.println("Parámetro 2:" + this.getServletContext().getInitParameter("admin"));
	}
 
}