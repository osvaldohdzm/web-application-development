package mx.ipn.escom.wad;

import java.io.IOException;
import java.io.PrintWriter;
 
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
 
public class InitParameters2 extends HttpServlet {
	private static final long serialVersionUID = 1L;
 
	protected void doGet(HttpServletRequest request,
		HttpServletResponse response) throws ServletException, IOException {
		
		String field1 = getField(request, "tiempoBloqueo", true);
		String field2 = getField(request, "numeroIntentos", false);
		String field3 = getField(request, "captchaHabilitado", false);
		
		PrintWriter out = response.getWriter();
		out.println("tiempoBloqueo:" + field1 );
		out.println("numeroIntentos:" + field2 );
		out.println("captchaHabilitado:" + field3 );
		//out.println("Parámetro 2:" + this.getServletContext().getInitParameter("admin"));
	}
	
	
	public String getField(HttpServletRequest request, String fieldName, boolean required) throws ServletException, IOException {
	    String fieldValue = getServletConfig().getInitParameter(fieldName);
	    if ( fieldValue == null || fieldValue.trim().isEmpty()) {
	        if (required) {	        	
	            throw new UnsupportedOperationException("Falta parametro de inicialización tiempoBloqueo");
	        } else {
	            fieldValue = null; // Make empty string null so that you don't need to hassle with equals("") afterwards.
	        }
	    }
	    return fieldValue;
	}
 
}