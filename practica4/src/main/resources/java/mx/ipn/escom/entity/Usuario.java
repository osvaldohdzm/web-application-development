package mx.ipn.escom.entity;

public class Usuario {
	private String nombre;
	private String pass;
	
	public Usuario(String nombre, String pass) {
		super();
		this.nombre = nombre;
		this.pass = pass;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	@Override
	public String toString() {
		return "Usuario [nombre=" + nombre + ", contraseña=" + pass + "]";
	}
	
	@Override
	public boolean equals(Object e) {
		if (e instanceof Usuario) {
			Usuario aux = (Usuario) e;
			if (aux.nombre.equals(this.nombre) && aux.pass.equals(this.pass))
				return true;
		}
		return false;
	}
}
