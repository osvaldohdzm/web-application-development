package mx.ipn.escom.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mx.ipn.escom.entity.Usuario;

/**
 * Servlet implementation class E3_Info
 */
public class E3_Info extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		PrintWriter out = response.getWriter();
		HttpSession session = request.getSession();
		Usuario user = (Usuario)session.getAttribute("usuario");
		if (user!=null) {
			out.println("<html>");
			out.println("<body>");
			out.println("<h1>Bienvenido "+user.getNombre()+"</h1>");
			out.println("</body>");
			out.println("</html>");
		}
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request,response);
	}

}
