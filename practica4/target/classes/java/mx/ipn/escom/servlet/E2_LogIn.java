package mx.ipn.escom.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mx.ipn.escom.entity.Usuario;

/**
 * Servlet implementation class E2_LogIn
 */
public class E2_LogIn extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private Usuario usuarioAceptado;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public E2_LogIn() {
        super();
        usuarioAceptado = new Usuario("necaxa","aguinaga");
        // TODO Auto-generated constructor stub
    }
    
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		String nombre = request.getParameter("nombre");
		String pass = request.getParameter("pass");
		Usuario usuarioDado = new Usuario(nombre,pass);
		if (usuarioDado.equals(usuarioAceptado)) {
			session.setAttribute("usuario", usuarioDado);
			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/E3_Info");
			dispatcher.forward(request, response);
		}
		else {
			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/");
			dispatcher.forward(request, response);
		}
	}

}
