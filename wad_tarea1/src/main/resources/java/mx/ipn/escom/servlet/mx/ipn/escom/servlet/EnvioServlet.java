package mx.ipn.escom.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class EnvioServlet
 */
public class EnvioServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		Enumeration<String> params = request.getParameterNames();
		String[] values;
		String paramName;
		PrintWriter out = response.getWriter();
		response.setContentType("text/html");
		out.println("<html>");
		out.println("<title>Tarea 3</title>");
		out.println("<body>");
		out.println("<h1>Envio dinámico de parámetros</h1>");
		out.println("<ul>");
		while(params.hasMoreElements()) {
			paramName=params.nextElement();
			values=request.getParameterValues(paramName);
			out.println("<li>"+paramName+"<ul>");
			for(String val : values)
				out.println("<li>"+val+"</li>");
			out.println("</ul></li>");
		}
		out.println("</ul>");
		out.println("</body>");
		out.println("</html>");
	}

}
