package mx.ipn.escom.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Service2
 */
public class E4_Service2 extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	private int cont;
	
	public void init() {
		cont=0;
	}
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("text/html");
		cont++;
		PrintWriter out = response.getWriter();
		out.println("<html>");
		out.println("<title>Hola Mundo practica 2</title>");
		out.println("<body>");
		out.println("<h1>Contador "+cont+"</h1>");
		out.println("</body>");
		out.println("</html>");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
